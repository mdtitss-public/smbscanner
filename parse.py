import requests
import json
import re
import datetime
import socket
import sqlite3
import argparse

def get_device_info_from_servicenow(identifier:str, base_url_servicenow:str):
    response = requests.get(base_url_servicenow+identifier)
    if response.status_code == 200:
        deviceinfo = json.loads(response.text)
        if float(deviceinfo["result"]["totalcount"]) == 1:
            output = deviceinfo["result"]["devices"][0]

            if deviceinfo["result"]["devices"][0]["last_logon"] != None:
                regex_result = re.search("(?<=\().*(?=\))", deviceinfo["result"]["devices"][0]["last_logon"])
                if regex_result != None:
                    output["last_logon_username"] = str(regex_result[0])

            return output
        else:
            return None

def determine_state(share:str, basic_auth_opensearch:str):
    check_for_existing = requests.get(basic_auth_opensearch+"/_doc/"+share["identifier"])
    myjson = json.loads(check_for_existing.text)
    if check_for_existing.status_code == 404:
        share["state"] = "open"
    elif check_for_existing.status_code == 200:
        share["state"] = myjson["_source"]["state"]
        if share["state"] == "closed":
            share["state"] == "open"
    else:
        print("statuscode:" +str(check_for_existing.status_code))
        raise SystemError("Opensearch returned an unexpected result")
    share["last_seen"] = datetime.datetime.utcnow().replace(microsecond=0).isoformat()
    return share

def update_single_share(state:str, identifier:str, basic_auth_opensearch:str):
    response = requests.post(basic_auth_opensearch+"/_update/"+identifier, data='{"doc": {"state" : "'+state+'"}}', headers={"content-type": "application/json", 'charset':'UTF-8'})
    if response.status_code < 200 or response.status_code > 300:
        print(f"Error sending data to opensearch: {str(response.text)}")
    else:
        print(f"Share {identifier} updated succesfully state: {state}")

def update_all_shares_for_servername(server_name:str, state:str, basic_auth_opensearch:str):
    myjson = {
        "script": {
            "source": "ctx._source.state = '"+ state +"'"
            },
            "query": {
                "match_phrase": {
                "host.name": server_name
                }
            }
        }
    response = requests.post(basic_auth_opensearch+"/_update_by_query", data=json.dumps(myjson), headers={"content-type": "application/json", 'charset':'UTF-8'})
    if response.status_code == 200:
        print("Closing all shares for server succeeded")
    if response.status_code != 200:
        print("Closing all shares for server FAILED")

def delete_old_shares(basic_auth_opensearch:str):
    myjson = {
                "query": {
                        "bool": {
                        "must_not": [
                            {
                            "match": {
                                "state": "open_share_accepted"
                            }
                            }
                        ],
                        "must_not": [
                            {
                            "range": {
                            "last_seen": {
                            "lt": "now-30d/d"
                            }
                            }
                        }
                        ]
                        }
                    }
                }
    response = requests.post(basic_auth_opensearch+"/_delete_by_query", data=json.dumps(myjson), headers={"content-type": "application/json", 'charset':'UTF-8'})
    if response.status_code == 200:
        print("Deleting old records succeeded")
    else:
        print("Deleting old records FAILED")

def get_hostname_for_ip(ip):
    try:
        data = socket.gethostbyaddr(ip)
        return data[0]
    except Exception:
        return ip

def flush_share(share:str, basic_auth_opensearch:str):
    print(f"Updating share: {share['identifier']}")
    share = determine_state(share, basic_auth_opensearch)
    response = requests.put(basic_auth_opensearch+"/_doc/"+share["identifier"], data=json.dumps(share), headers={"content-type": "application/json", 'charset':'UTF-8'})
    if (response.status_code < 200 or response.status_code > 300) and response.status_code != 409:
        print("Error sending data to opensearch: " + str(response.text))

def parse_log(file:str, basic_auth_opensearch:str, basic_auth_servicenow:str):
    con = sqlite3.connect(file)
    query = f"select name, ip, computerid, os, domain, remark, smbv1, read, write from shares left join computers on computers.hostname = shares.computerid where (shares.read = 1 or shares.write = 1) and lower(name) not in ('print$', 'netlogon', 'sysvol');"
    cur = con.cursor()
    shares = cur.execute(query).fetchall()
    for row in shares:
        try:
            share = {}
            share["share"] = row[0].lower()
            share["host"] = { 
                            "ip" : row[1],
                            "name" : row[2].lower(),
                            "domain":  row[4].lower(),
                            "wurclient" : False,
                            "wurserver" : False
                            }
            share["os"] = {"full" : row[3]}
            
            share["share"] = {
                "name" : row[0].lower() if row[0] != "(NONE)" else get_hostname_for_ip(row[1]),
                "read" : True if row[7] == 1 else False,
                "write" : True if row[8] == 1 else False,
                "remark" : row[5],
                "smb_v1" : row[6]
            }
            share["identifier"] = share["host"]["name"]+"-"+share["share"]["name"]

            if (re.search("^(d|l)([0-9]+)", share["host"]["name"] ) != None):
                share["host"]["wurclient"] = True
            if "scomp" in share["host"]["name"]:
                share["host"]["wurserver"] = True

            servicenow_info = None
            if share["host"]["wurclient"] or share["host"]["wurserver"]:
                servicenow_info = get_device_info_from_servicenow(share["host"]["name"].split(".")[0], basic_auth_servicenow)

            if servicenow_info != None:
                share["servicenow_info"] = servicenow_info
                share["os"]["full"] = servicenow_info["os"]
                share["os"]["version"] = servicenow_info["os_version"]
            
            flush_share(share, basic_auth_opensearch)
        except IndexError:
            print(f"Failed to index row: {row}")

def main():
    parser = argparse.ArgumentParser(description="Scripting to scan SMB-shares")
    
    parser.add_argument('-action',required=True,choices={"parse-cme-output", "update-single-share", "update-all-shares-for-host"}, help="What do you want me to do?")
    parser.add_argument('-state',required=False,choices={"closed", "open_share_accepted"}, help="To which state do you want to update the share?")
    parser.add_argument('-item', required=False, help="For a single share: document id from opensearch. For all shares on a host: host.name from opensearch")
    parser.add_argument('-base_url_opensearch', required=True, help="https://USER:PASSWORD@URL:PORT/log-shares-smb")
    parser.add_argument('-base_url_servicenow', required=True, help="https://USER:PASSWORD@wur.service-now.com/api/uw/wur_device_info/get_device_details/")

    args = parser.parse_args()
    action = args.action
    item = args.item
    state = args.state
    base_url_opensearch = args.base_url_opensearch
    base_url_servicenow = args.base_url_servicenow

    if action == "parse-cme-output":
        parse_log("cmeworkdir/workspaces/default/smb.db", base_url_opensearch, base_url_servicenow)
        delete_old_shares(base_url_opensearch)
    if action == "update-single-share":
        if item and state:
            update_single_share(state, item, base_url_opensearch)
        else:
            print("item and state required")
    if action == "update-all-shares-for-host":
        if item and state:
            update_all_shares_for_servername(item, state, base_url_opensearch)
        else:
            print("item and state required")
        

if __name__ == "__main__":
    main()