#!/bin/bash
DIR="$( cd "$( dirname "$0" )" && pwd )"
rm $DIR/cmeworkdir/* -Rf
$DIR/masscan -p 445 --rate=400000 137.224.0.0/16 -oL - | awk '{print $4}' >> $DIR/cmeworkdir/hosts.up
$DIR/masscan -p 445 --rate=400000 10.0.0.0/8 -oL - | awk '{print $4}' >> $DIR/cmeworkdir/hosts.up
sed -i '/^$/d' $DIR/cmeworkdir/hosts.up
docker run --rm -it -v $DIR/cmeworkdir:/root/.cme --network host cme-local smb /root/.cme/hosts.up -u ADUSER -p ADPASSWORD --shares
python3 $DIR/parse.py -a parse-cme-output -base_url_opensearch https://USER:PASSWORD@OPENSEARCHURL/log-shares-smb -base_url_servicenow https://USER:PASSWORD@wur.service-now.com/api/uw/wur_device_info/get_device_details/