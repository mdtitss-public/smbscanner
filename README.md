Dependencies: docker on the host, crackmapexec (in docker container, see below), masscan (see below), opensearch/elasticsearch

Clone the crackmapexec git repo and build a docker container from it:

docker build --network host -t cme-local .

Then build masscan, clone the gitrepo and place the generated executable in this directory:
cd masscan
make 
copy bin/masscan ..

Install the python dependencies:
pip3 install -r requirements.txt

Replace the user credentials for crackmapexec in the start.sh script (needs to be a valid AD user with very limited access)